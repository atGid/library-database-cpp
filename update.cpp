//

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <map>
#include <iomanip>

using namespace std;

typedef char String[25];
enum TransactionType {Add, Delete, ChangeOnHand, ChangePrice};

struct BookRec
{
	unsigned int isbn;
	String name;
	String author;
	int onhand;
	float price;
	String type;
};

struct TransactionRec
{
	TransactionType ToDo;
	BookRec B;
};

void printRec(BookRec book);
void readFile(string filename);
void addTransactions(string trans, string master, map<unsigned int, long> &lib);
void buildMap(map<unsigned int, long> &library);

int main(int argc, char * argv [])
{
	if(argc < 4)
		return 1;
	string masterFile = argv[1];
	string transFile = argv[2];
	string newFile = argv[3];
	string cpy = "cp " + masterFile + " copy.out"; //Create intermediate copy file to work with
	system(cpy.c_str());

	map<unsigned int, long> lib;
	buildMap(lib);
	addTransactions(transFile, newFile, lib);
	system("clear");
	cout << endl << setw(53) << "UPDATED MASTER FILE" << endl
		<< setw(53) << "~~~~~~~~~~~~~~~~~~~";
	readFile(newFile);

	system("rm copy.out"); //Delete intermediate copy file
	return 0;
}

//Prints given book record in pretty format
void printRec(BookRec book)
{
        cout << " ";
        //Format ISBN
        for(int i = 1; i <= 1000000000; i*=10)
        {
                if((book.isbn / i) < 1)
                        cout << "0";
        }
        cout << book.isbn << " ";
        //Name
        cout << setw(25) << book.name << " ";
        //Author
        cout << setw(25) << book.author << " ";
        //Count
        cout << setw(3) << book.onhand << " ";
        //Format Price
        cout.setf(ios::fixed, ios::floatfield);
        cout.precision(2);
        cout << setw(6) << book.price;
        //Type
        cout << setw(10) << book.type;

        cout << endl;
}

//Reads given file for books and prints them
void readFile(string filename)
{
        fstream binfile(filename.c_str(), ios::in | ios::binary);
        BookRec book;

        cout << endl << endl;
        while(binfile.read((char *) &book, sizeof(book)))
        {
                printRec(book);
        }
        cout << endl;
}

//Loops through each transaction and the master file (copy) to make changes and 
// represent them in the updated file.
void addTransactions(string trans, string master, map <unsigned int, long> &lib) //Could use some refactoring
{
	fstream infile("copy.out", ios::in | ios::binary);	//File contains all old master records
	fstream transFile(trans.c_str(), ios::in | ios::binary);	//File contains all transactions
	fstream updateFile(master.c_str(), ios::out | ios::binary);	//File holds new bookrec output
	fstream errorFile("ERRORS", ios::out);	//File holds all errors

	BookRec m;	//current master record
	TransactionRec t; //current transaction record
	BookRec newBook; //book to be added to updated file
	int transCount = 0; //Count of transactions
	infile.read((char *) &m, sizeof(m));
	while(transFile.read( (char*) &t, sizeof(t))) //Keep looping while transactions exist
	{
		transCount+=1;
		//Check if curr book isbn is less than curr transaction
		// if so, add all books up to transaction isbn
		while(t.B.isbn > m.isbn && infile.good())
                {
                        newBook = m;
                        updateFile.write((char*) &newBook, sizeof(newBook));
                        infile.read((char *) &m, sizeof(m));
                }
		//If a duplicate book is found, call switch case to act on record
		if(lib.find(t.B.isbn) != lib.end())
		{
			//Now check for transaction type and act accordingly
			switch(t.ToDo)
			{
				case 0: //Add
					errorFile << "Error in transaction number " << transCount << ": cannot add-duplicate key "
						<< t.B.isbn << '\n';
					//Write original duplicate to new file without making changes from transaction
					newBook = m;
					updateFile.write((char*) &newBook, sizeof(newBook));
					break;
				case 1: //Delete
					lib.erase(lib.find(t.B.isbn));
					break;
				case 2: //ChangeOnHand
					infile.seekg(lib.lower_bound(t.B.isbn)->second);
					infile.read((char*) &m, sizeof(m));
					newBook = m;
					newBook.onhand += t.B.onhand;
					if(newBook.onhand >= 0)
						updateFile.write((char*) &newBook, sizeof(newBook));
					else
					{
						errorFile << "Error in transaction number " << transCount << ": count = " << newBook.onhand
							<< " for key " << newBook.isbn << '\n';
						newBook.onhand = 0;
						updateFile.write((char*) &newBook, sizeof(newBook));
					}
					break;
				case 3: //Change Price
					infile.seekg(lib.lower_bound(t.B.isbn)->second);
					infile.read((char*) &m, sizeof(m));
					newBook = m;
					newBook.price = t.B.price;
					updateFile.write((char*) &newBook, sizeof(newBook));
					break;
			}
			infile.read((char *) &m, sizeof(m));
		}
		else	//If transaction is not a match to map
		{
			//Now check transaction type and act accordingly. 
			switch(t.ToDo)
			{
				case 0: //Add
					lib.insert(pair<unsigned int, long>(t.B.isbn, (long)updateFile.tellg()));
					updateFile.write((char*) &t.B, sizeof(t.B));
                                        break;
                                case 1: //Delete
					errorFile << "Error in transaction number " << transCount << ":cannot delete-no such key "
						<< t.B.isbn << '\n';
                                        break;
                                case 2: //ChangeOnhand
					errorFile << "Error in transaction number " << transCount << ": cannot change count-no such key "
						<< t.B.isbn << '\n';
                                        break;
                                case 3: //ChangePrice
					errorFile << "Error in transaction number " << transCount << ": cannot change price-no such key "
						<< t.B.isbn << '\n';
                                        break;

			}
		}
	}
	//Read remaining files from master
	while(infile.read((char*) &m, sizeof(m)))
	{
		newBook = m;
		updateFile.write((char*) &newBook, sizeof(newBook));
	}
}

//Take original master file and build map of isbn numbers and byte locations
void buildMap(map<unsigned int, long> &library)
{
	fstream infile("copy.out", ios::in | ios::binary);
	BookRec book;

	while(infile.read((char *) &book, sizeof(book)))
	{
		library.insert(pair<unsigned int, long>(book.isbn, (infile.tellg() - (long)sizeof(book))));
	}

}

