//Gideon Ojo	CSC310 Prog1	Part2

#include <iostream>
#include <fstream>
#include <sstream>
#include <string.h>
#include <iomanip>

using namespace std;

typedef char String[25];
struct BookRec
{
	unsigned int isbn;
	String name;
	String author;
	int onhand;
	float price;
	String type;
};

void printRec(BookRec book);
void inputFile(string file);
void readFile();

int main(int argc, char *argv[])
{
	string fileName = argv[1];

	inputFile(fileName);
	readFile();
	return 0;
}

//Format Book for neat printing
void printRec(BookRec book)
{
	cout << " ";
	//Format ISBN
	for(int i = 1; i <= 1000000000; i*=10)
	{
		if((book.isbn / i) < 1)
			cout << "0";
	}
	cout << book.isbn << " ";
	//Name
	cout << setw(25) << book.name << " ";
	//Author
        cout << setw(25) << book.author << " ";
	//Count
	cout << setw(3) << book.onhand << " ";
	//Format Price
	cout.setf(ios::fixed, ios::floatfield);
	cout.precision(2);
	cout << setw(6) << book.price;
	//Type
	cout << setw(10) << book.type;

	cout << endl;
}

//Read book record from recorded file and print
void readFile()
{
	fstream binfile("library.out", ios::in | ios::binary);
	BookRec book;
	
	cout << endl << endl;
	while(binfile.read((char *) &book, sizeof(book)))
	{
		printRec(book);
	}
	cout << endl;
}

//Take input text file and convert to binary outfile
void inputFile(string file)
{
	fstream infile (file.c_str(), ios::in);
	fstream outfile("library.out", ios::out | ios::binary);

	BookRec book;
	bool log = true; //Bool = true if book is to be logged. False if ignored
	bool wOut = false; //Bool = true if book is to be written to cout
	int lineN = 1; //Count of records
	long int isbn;
	cout << endl;
	while(infile >> isbn)
	{
		if(isbn < 0)
		{
			cerr << "~~~ Illegal isbn number at line " << lineN << ". Record ignored. ~~~" << endl;
			log = false;
		}
		if(book.isbn > isbn && log) //Add log consideration so that illegal number is not rewritten as out of seq
		{
			cerr << "~~~ Isbn number out of sequence at line " << lineN << " of " 
				<< file << ". ~~~" << endl;
			wOut = true;
		}
		if(log)	
			book.isbn = (unsigned int) isbn; //Dont add ISBN to sequence if number is illegal
		infile.seekg(1, ios::cur); //Position pointer ahead of | delimeter
		//Add string contents of book
		infile.getline(book.name, sizeof(String), '|');
		infile.getline(book.author, sizeof(String), '|');
		//Add numerical contents of book
		infile >> book.onhand; 
		infile.seekg(1, ios::cur);
		//Write to cerr if there is an illegal count of books
		if(book.onhand < 0)
		{
			cerr << "~~~ Negative amount of books onhand on line " << lineN << " of " << file 
				<< ". Record ignored.~~~" << endl;
			log = false;
			wOut = true;
		}
		infile >> book.price;
		infile.seekg(1, ios::cur);
		//Write to cerr if there is an illegal price
		if(book.price < 0)
		{
			cerr << "~~~ Negative book price on line " << lineN << " of " << file 
				<< ". Record ignored.~~~" << endl;
			log = false;
			wOut = true;
		}
		infile.getline(book.type, sizeof(String), '\n');

		if(log)
		{
			outfile.write((char *) &book, sizeof(BookRec));
		}
		if(wOut)
		{
			printRec(book);
		}

		wOut = false;
		log = true;
		lineN += 1;
	}
}
