# Library Database Cpp

THE DATABASE  - update.cpp

Description:
  The database program allows anyone to update or make any changes to files in their library automatically.
  The program is useful by providing users ways to add files to library or remove items while keeping the library sorted by isbn number.
  
Parameters:
  - Input:
  The program requires three binary files in the particular order defined below.
  1. Master library. The file that contains all current records in the user library.
  2. Transactions. The file that contains all transactions that are to be made to the user library.
  3. Output library. The file that will store the updated library including all changes
  
  - Output:
  The program will output two files that the user can reference. The first file is an ERRORS file that will contain any and all errors that occur during the transaction.
  The second file is the output library that was one of the input parameters. This file will be returned as an updated binary library of all records.
  
Algorithm:
  The program runs a simple algorithm to make sure all the books and transactions are kept in order. The algorithm is as defined below.
  1. Map all books in the master library by ISBN number and byte location. This will be used later to verify replicates.
  2. Simultaneously open master and transaction file for reading so that information can be mapped to new output library.
  3. Read the initial master library book.
  4. Read the intial transaction book.
  5. Add all master book records with ISBN numbers lower than the current transaction record to the output library
  6. Check if transaction record is a duplicate using the map
    a. If the transaction is a duplicate, follow action of transaction type and increment to the next master book record
    b. If the transaction is NOT a duplicate, follow action of non matched transaction types
    c. Report all errors
  7. If there are more transactions, go back to step 4.
  8. Add remaining master file records to output library
  This algorithm is made efficient by not storing all book files in memory at one time. The algorithm only requires that the user have a map of all master book records and 2 book records (including transaction book) while making updates to the output library.
  
  Gideon Ojo  Mercer University  CSC310  January 29th, 2019